package reform_repository

import (
	"fmt"
)

type QueryBuilderWithIDOnly struct {
	ID *int
}

func (f QueryBuilderWithIDOnly) Select() *[]string {
	return nil
}

func (f QueryBuilderWithIDOnly) Join() *string {
	return nil
}

func (f QueryBuilderWithIDOnly) Where() string {
	sql := "true"
	if f.ID != nil && *f.ID != 0 {
		sql += fmt.Sprintf(" AND id = %v", *f.ID)
	}
	return sql
}

func (qb QueryBuilderWithIDOnly) GroupBy() *[]string {
	return nil
}

func (qb QueryBuilderWithIDOnly) OrderBy() *string {
	return nil
}

func (qb QueryBuilderWithIDOnly) Pagination() *struct {
	Page    int
	PerPage int
} {
	return nil
}
