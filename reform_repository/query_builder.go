package reform_repository

type QueryBuilder interface {
	Select() *[]string
	Where() string
	Join() *string
	OrderBy() *string
	GroupBy() *[]string
	Pagination() *struct {
		Page    int
		PerPage int
	}
}
