package reform_repository

import (
	"database/sql"
	"fmt"
	"os"
	"strings"
	"sync"
	"time"

	"gopkg.in/reform.v1"
)

type GenericModel = reform.Record

// Problem 1: can't use reform.Record as T
//
type Repository[T interface{}, T2 *T] struct {
	DB           *reform.DB
	View         reform.View
	TestMocks    *[]reform.Struct
	EnableLog    bool
	SingleWrites bool
	_mu          sync.Mutex
	Unpack       *func(T2) error
	Pack         *func(T2) error
}

func (repo *Repository[T, T2]) IsTestModeOn() bool {
	isEnv := strings.ToUpper(os.Getenv("ENV")) == "TEST" || os.Getenv("TEST") == "1"
	return isEnv // && repo.TestMocks != nil
}

func (repo *Repository[T, T2]) GetTestMocks() *[]reform.Struct {
	if repo.TestMocks != nil {
		return repo.TestMocks
	}
	return &[]reform.Struct{}
}

func (repo *Repository[T, T2]) Mock(reformRecords []interface{}) error {
	structs := []reform.Struct{}
	for _, reformRecord := range reformRecords {
		_struct, ok := (reformRecord).(reform.Struct)
		if !ok {
			err := fmt.Errorf("Can not cast %T (%+v) to reform.Struct", reformRecord, reformRecord)
			panic(err)
			return err
		}
		fmt.Printf("[Repository] Mocking %T (%+v)\n", reformRecord, _struct)
		structs = append(structs, _struct)
	}
	repo.TestMocks = &structs
	return nil
}

func (repo *Repository[T, T2]) Select(qb QueryBuilder) ([]T, error) {
	records, err := repo.__select(qb)
	if err != nil {
		return nil, err
	}
	models, err := repo.UnmarshalRecords(records)
	if err != nil {
		return nil, err
	}
	return models, nil
}

func (repo *Repository[T, T2]) Select2(qb QueryBuilder) ([]T, error) {
	records, err := repo.__select2(qb)
	if err != nil {
		return nil, err
	}
	models, err := repo.UnmarshalRecords(records)
	if err != nil {
		return nil, err
	}
	return models, nil
}

func (repo *Repository[T, T2]) SelectFirst(qb QueryBuilder) (*T, error) {
	record, err := repo.__selectFirst(qb)
	if err != nil {
		return nil, err
	}
	model, err := repo.UnmarshalRecord(record)
	if err != nil {
		return nil, err
	}
	return model, nil
}

func (repo *Repository[T, T2]) SelectSingle(qb QueryBuilder) (*T, error) {
	models, err := repo.Select(qb)
	if err != nil {
		return nil, err
	}
	switch len(models) {
	case 1:
		{
			model := models[0]
			return &model, nil
		}
	case 0:
		return nil, nil
	default:
		return nil, fmt.Errorf("Found %v records but expect at most one", len(models))
	}
}

func (repo *Repository[T, T2]) SelectOne(id int) (*T, error) {
	model, err := repo.SelectFirst(QueryBuilderWithIDOnly{ID: &id})
	if err != nil {
		return nil, err
	}
	return model, nil
}

// Would love to have generics support in Go

func (repo *Repository[T, T2]) UnmarshalRecord(record *reform.Struct) (*T, error) {
	if record == nil {
		return nil, nil
	}

	model, ok := (*record).(T2)
	if !ok {
		return nil, fmt.Errorf("Can not cast record %+v to T", *record)
	}

	if repo.Unpack != nil {
		if err := (*repo.Unpack)(model); err != nil {
			return nil, err
		}
	}

	// Problem 2: since T is any - no way to cast properly
	//
	var t interface{} = *model
	var v T = t.(T)

	return &v, nil
}

func (repo *Repository[T, T2]) UnmarshalRecords(records *[]reform.Struct) ([]T, error) {
	models := []T{}
	for _, record := range *records {
		model, err := repo.UnmarshalRecord(&record)
		if err != nil {
			return []T{}, err
		} else if model != nil {
			models = append(models, *model)
		}
	}
	return models, nil
}

func (repo *Repository[T, T2]) Save(model reform.Record) error {
	// [TEST MOCK SYSTEM INTERCEPTION]
	if repo.IsTestModeOn() {
		return fmt.Errorf("[Repository] Attempt to save %+v in TEST env", model)
	}
	// [END OF TEST MOCK SYSTEM INTERCEPTION]

	if repo.Pack != nil {
		v := model.(T2)
		if err := (*repo.Pack)(v); err != nil {
			return err
		}
	}

	if repo.EnableLog {
		fmt.Printf("[SQL] [%v] [%v] SAVE: %v\n", time.Now().Format("2006-01-02T15:04:05Z07:00"), repo.View.Name(), model)
	}

	if repo.SingleWrites {
		repo._mu.Lock()
		defer repo._mu.Unlock()
	}

	return repo.DB.Save(model)
}

func (repo *Repository[T, T2]) Save2(model T2) error {
	// [TEST MOCK SYSTEM INTERCEPTION]
	if repo.IsTestModeOn() {
		return fmt.Errorf("[Repository] Attempt to save %+v in TEST env", model)
	}
	// [END OF TEST MOCK SYSTEM INTERCEPTION]

	if repo.Pack != nil {
		if err := (*repo.Pack)(model); err != nil {
			return err
		}
	}

	if repo.EnableLog {
		fmt.Printf("[SQL] [%v] [%v] SAVE2: %v\n", time.Now().Format("2006-01-02T15:04:05Z07:00"), repo.View.Name(), model)
	}

	if repo.SingleWrites {
		repo._mu.Lock()
		defer repo._mu.Unlock()
	}

	// Problem 2: since T is any - no way to cast properly
	//
	var t interface{} = model
	var v reform.Record = t.(reform.Record)

	return repo.DB.Save(v)
}

func (repo *Repository[T, T2]) __select2(queryBuilder QueryBuilder) (*[]reform.Struct, error) {
	// [TEST MOCK SYSTEM INTERCEPTION]
	if repo.IsTestModeOn() {
		return repo.GetTestMocks(), nil
	}
	// [END OF TEST MOCK SYSTEM INTERCEPTION]

	QUERY := ""

	// SELECT
	if selectFields := queryBuilder.Select(); selectFields != nil {
		QUERY = fmt.Sprintf("SELECT %v FROM %v", strings.Join(*selectFields, ", "), repo.View.Name())
	} else {
		QUERY = fmt.Sprintf("SELECT * FROM %v", repo.View.Name())
	}

	if join := queryBuilder.Join(); join != nil {
		QUERY += " " + *join
	}

	if where := queryBuilder.Where(); len(where) > 0 {
		QUERY += " WHERE " + where
	}
	if groupBy := queryBuilder.GroupBy(); groupBy != nil && len(*groupBy) > 0 {
		QUERY += " GROUP BY " + strings.Join(*groupBy, ",")
	}
	if order := queryBuilder.OrderBy(); order != nil {
		QUERY += " ORDER BY " + *order
	}

	if pagination := queryBuilder.Pagination(); pagination != nil {
		offset := (pagination.Page - 1) * pagination.PerPage
		QUERY += fmt.Sprintf(" LIMIT %v OFFSET %v", pagination.PerPage, offset)
	}

	if repo.EnableLog {
		fmt.Printf("[SQL] [%v] [%v] %v\n", time.Now().Format("2006-01-02T15:04:05Z07:00"), repo.View.Name(), QUERY)
	}

	rows, err := repo.DB.Query(QUERY)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	structs := []reform.Struct{}
	for rows.Next() {
		_struct := repo.View.NewStruct()
		pointers := _struct.Pointers()
		if err = rows.Scan(pointers...); err != nil {
			return nil, err
		}
		structs = append(structs, _struct)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return &structs, nil
}

func (repo *Repository[T, T2]) __select(queryBuilder QueryBuilder) (*[]reform.Struct, error) {
	// [TEST MOCK SYSTEM INTERCEPTION]
	if repo.IsTestModeOn() {
		return repo.GetTestMocks(), nil
	}
	// [END OF TEST MOCK SYSTEM INTERCEPTION]

	tail := ""
	if where := queryBuilder.Where(); len(where) > 0 {
		tail += " WHERE " + where
	}
	if groupBy := queryBuilder.GroupBy(); groupBy != nil && len(*groupBy) > 0 {
		tail += " GROUP BY " + strings.Join(*groupBy, ",")
	}
	if order := queryBuilder.OrderBy(); order != nil {
		tail += " ORDER BY " + *order
	}

	if pagination := queryBuilder.Pagination(); pagination != nil {
		offset := (pagination.Page - 1) * pagination.PerPage
		tail += fmt.Sprintf(" LIMIT %v OFFSET %v", pagination.PerPage, offset)
	}

	if repo.EnableLog {
		fmt.Printf("[SQL] [%v] [%v] SELECT ALL FROM: %v\n", time.Now().Format("2006-01-02T15:04:05Z07:00"), repo.View.Name(), tail)
	}

	rows, err := repo.DB.SelectAllFrom(repo.View, tail)
	if err != nil {
		return nil, err
	}

	return &rows, nil
}

func (repo *Repository[T, T2]) __selectFirst(queryBuilder QueryBuilder) (*reform.Struct, error) {
	// [TEST MOCK SYSTEM INTERCEPTION]
	if repo.IsTestModeOn() {
		first := (*repo.GetTestMocks())[0]
		return &first, nil
	}
	// [END OF TEST MOCK SYSTEM INTERCEPTION]

	whereSQL := queryBuilder.Where()
	tail := fmt.Sprintf("WHERE %v", whereSQL)

	if repo.EnableLog {
		fmt.Printf("[SQL] [%v] [%v] SELECT ONE FROM: %v\n", time.Now().Format("2006-01-02T15:04:05Z07:00"), repo.View.Name(), tail)
	}

	row, err := repo.DB.SelectOneFrom(repo.View, tail)
	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return &row, nil
}

func (repo *Repository[T, T2]) Destroy(record reform.Record) error {
	// [TEST MOCK SYSTEM INTERCEPTION]
	if repo.IsTestModeOn() {
		return fmt.Errorf("[Repository] Attempt to destroy %+v in TEST env", record)
	}
	// [END OF TEST MOCK SYSTEM INTERCEPTION]

	if repo.EnableLog {
		fmt.Printf("[SQL] [%v] [%v] DESTROY: %v\n", time.Now().Format("2006-01-02T15:04:05Z07:00"), repo.View.Name(), record)
	}

	if repo.SingleWrites {
		repo._mu.Lock()
		defer repo._mu.Unlock()
	}

	return repo.DB.Delete(record)
}

func (repo *Repository[T, T2]) DestroyAll(queryBuilder QueryBuilder) error {
	// [TEST MOCK SYSTEM INTERCEPTION]
	if repo.IsTestModeOn() {
		return fmt.Errorf("[Repository] Attempt to destroy all with %+v in TEST env", queryBuilder)
	}
	// [END OF TEST MOCK SYSTEM INTERCEPTION]

	whereSQL := queryBuilder.Where()
	tail := fmt.Sprintf("WHERE %v", whereSQL)

	if repo.EnableLog {
		fmt.Printf("[SQL] [%v] [%v] DELETE FROM: %v\n", time.Now().Format("2006-01-02T15:04:05Z07:00"), repo.View.Name(), tail)
	}

	if repo.SingleWrites {
		repo._mu.Lock()
		defer repo._mu.Unlock()
	}

	_, err := repo.DB.DeleteFrom(repo.View, tail)
	return err
}

func (repo *Repository[T, T2]) UpdateAll(record reform.Record, columns []string, queryBuilder QueryBuilder) error {
	// [TEST MOCK SYSTEM INTERCEPTION]
	if repo.IsTestModeOn() {
		return fmt.Errorf("[Repository] Attempt to update %v for all with %+v in TEST env", columns, queryBuilder)
	}
	// [END OF TEST MOCK SYSTEM INTERCEPTION]

	whereSQL := queryBuilder.Where()
	tail := fmt.Sprintf("WHERE %v", whereSQL)

	if repo.EnableLog {
		fmt.Printf("[SQL] [%v] [%v] UPDATE ALL: %v %v\n", time.Now().Format("2006-01-02T15:04:05Z07:00"), repo.View.Name(), columns, tail)
	}

	if repo.SingleWrites {
		repo._mu.Lock()
		defer repo._mu.Unlock()
	}

	_, err := repo.DB.UpdateView(record, columns, tail)
	return err
}

func (repo *Repository[T, T2]) DestroyAllWithoutDeadlock(queryBuilder QueryBuilder) error {
	// Strategy A (now work in mysql 5.6):
	// whereSQL := queryBuilder.Where()
	// querySQL := fmt.Sprintf("DELETE FROM %v WHERE id IN (SELECT id FROM %v WHERE %v ORDER BY id)", repo.View.Name(), repo.View.Name(), whereSQL)
	// if _, err := repo.DB.Exec(querySQL); err != nil {
	// 	return err
	// }
	// return nil

	// Strategy B
	{
		ids := []int{}
		{
			query := fmt.Sprintf("SELECT id FROM %v WHERE %v", repo.View.Name(), queryBuilder.Where())
			if rows, err := repo.DB.Query(query); err == nil {
				for rows.Next() {
					id := 0
					if err := rows.Scan(&id); err != nil {
						return err
					}
					ids = append(ids, id)
				}
			} else {
				return err
			}
		}
		if len(ids) == 0 {
			return nil
		}

		escaped := []string{}
		for _, id := range ids {
			escaped = append(escaped, fmt.Sprintf("%v", id))
		}

		querySQL := fmt.Sprintf("DELETE FROM %v WHERE id IN (%v)", repo.View.Name(), strings.Join(escaped, ","))
		if _, err := repo.DB.Exec(querySQL); err != nil {
			return err
		}
		return nil
	}
}

func (repo *Repository[T, T2]) Count(qb QueryBuilder) (*int, error) {
	joinSql := ""
	if qbJoin := qb.Join(); qbJoin != nil {
		joinSql = *qbJoin
	}

	query := fmt.Sprintf(
		"SELECT COUNT(`%v`.id) as count FROM %v %v WHERE %v",
		repo.View.Name(),
		repo.View.Name(),
		joinSql,
		qb.Where())

	if repo.EnableLog {
		fmt.Printf("[SQL] [%v] [%v] COUNT: %v\n", time.Now().Format("2006-01-02T15:04:05Z07:00"), repo.View.Name(), query)
	}

	row := repo.DB.QueryRow(query)
	count := 0
	switch err := row.Scan(&count); err {
	case sql.ErrNoRows:
		return &count, nil
	case nil:
		return &count, nil
	default:
		return nil, err
	}
}
