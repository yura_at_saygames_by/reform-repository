package reform_repository

import "reflect"

// Took from: https://mangatmodi.medium.com/go-check-nil-interface-the-right-way-d142776edef1
//
func isNilFixed(i interface{}) bool {
	if i == nil {
		return true
	}
	switch reflect.TypeOf(i).Kind() {
	case reflect.Ptr, reflect.Map, reflect.Array, reflect.Chan, reflect.Slice:
		return reflect.ValueOf(i).IsNil()
	}
	return false
}

func ToReferences[T any](originalSlice []T) []*T {
	result := make([]*T, len(originalSlice))
	for _, v := range originalSlice {
		clone := v
		result = append(result, &clone)
	}
	return result
}

func ToValues[T any](originalSlice []*T) []T {
	result := make([]T, len(originalSlice))
	for _, v := range originalSlice {
		result = append(result, *v)
	}
	return result
}
