package reform_repository

import (
	"database/sql/driver"
	"encoding/json"
	"reflect"
	"sort"
)

type StringList []string

func NewStringList(list []string) StringList {
	sort.Strings(list)
	return StringList(list)
}

func (list StringList) Value() (driver.Value, error) {
	// Varian 1
	jsonBytes, err := json.Marshal(list)
	if err != nil {
		return nil, err
	}
	return jsonBytes, nil
}

func (list *StringList) Scan(value interface{}) error {
	if isNilFixed(value) {
		*list = StringList([]string{})
		return nil
	}

	valueStr := string(value.([]uint8))

	valueStrings := []string{}
	if err := json.Unmarshal([]byte(valueStr), &valueStrings); err != nil {
		return err
	}

	*list = StringList(valueStrings)
	return nil
}

// O(log N). List should be sorted ascendentally
func (list StringList) Includes(x string) bool {
	n := len(list)
	i := sort.Search(n, func(i int) bool {
		return (list)[i] >= x
	})
	if i < n && (list)[i] == x {
		return true
	}
	return false
}

func (list StringList) Intersection(another StringList) StringList {
	intersected := []string{}
	for _, candidate := range list {
		if another.Includes(candidate) {
			intersected = append(intersected, candidate)
		}
	}
	return NewStringList(intersected)
}

func (list StringList) Subtract(another StringList) StringList {
	result := []string{}
	for _, candidate := range list {
		if another.Includes(candidate) {
			continue
		}
		result = append(result, candidate)
	}
	return NewStringList(result)
}

func (list StringList) Equal(another StringList) bool {
	return reflect.DeepEqual(list, another)
}
