package reform_repository

import "github.com/go-sql-driver/mysql"

// https://stackoverflow.com/questions/38493985/how-to-parse-mysql-error-golang
//
func IsDuplicateEntryError(err error) bool {
	mysqlErr, ok := err.(*mysql.MySQLError)
	if !ok {
		return false
	}
	return mysqlErr.Number == 1062
}
