package reform_repository

import (
	"fmt"
	"strings"

	"gopkg.in/reform.v1"
)

type Iterator[T interface{}, T2 *T] struct {
	Repo         Repository[T, T2]
	QueryBuilder QueryBuilder
	lastID       int
	done         bool
}

func (i Iterator[T, T2]) HasMore() bool {
	return i.done == false
}

func (i *Iterator[T, T2]) Next() (*[]reform.Struct, error) {
	// [TEST MOCK SYSTEM INTERCEPTION]
	if i.Repo.IsTestModeOn() {
		i.done = true
		mocks := i.Repo.GetTestMocks()
		return mocks, nil
	}
	// [END OF TEST MOCK SYSTEM INTERCEPTION]

	tail := ""
	if where := i.QueryBuilder.Where(); len(where) > 0 {
		tail += " WHERE " + where
	}
	if i.lastID != 0 {
		tail += fmt.Sprintf(" AND id > %v", i.lastID)
	}

	if groupBy := i.QueryBuilder.GroupBy(); groupBy != nil && len(*groupBy) > 0 {
		tail += " GROUP BY " + strings.Join(*groupBy, ",")
	}

	tail += " ORDER BY id ASC"
	tail += " LIMIT 10000"

	structs, err := i.Repo.DB.SelectAllFrom(i.Repo.View, tail)
	if err != nil {
		return nil, err
	}

	if len(structs) > 0 {
		lastStruct := structs[len(structs)-1]
		// asserting PK is the first volumn value
		pkValue := lastStruct.Values()[0]
		if pk, ok := pkValue.(int); ok {
			i.lastID = pk
		} else {
			return nil, fmt.Errorf("Can not cast first column value to int (as PK)")
		}
	} else {
		i.done = true
	}

	return &structs, nil
}
