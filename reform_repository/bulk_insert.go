package reform_repository

import (
	"fmt"
	"time"

	"gopkg.in/reform.v1"
)

func BulkInsert[T reform.Record](DB *reform.DB, items []T) error {
	batchSize := 1000

	structs := make([]reform.Struct, len(items))
	for _, t := range items {
		clone := t
		structs = append(structs, clone)
	}

	fmt.Printf("[SQL] [%v] BULK INSERT: %v items, batch size %v\n", time.Now().Format("2006-01-02T15:04:05Z07:00"), len(structs), batchSize)

	for i := 0; i < len(structs)/batchSize+1; i++ {
		low, high := i*batchSize, (i+1)*batchSize
		if high > len(structs) {
			high = len(structs)
		}
		batch := structs[low:high]
		if err := DB.InsertMulti(batch...); err != nil {
			return err
		}
	}

	return nil
}

func BulkInsert2(DB *reform.DB, structs []reform.Struct) error {
	batchSize := 1000

	fmt.Printf("[SQL] [%v] BULK INSERT 2: %v items, batch size %v\n", time.Now().Format("2006-01-02T15:04:05Z07:00"), len(structs), batchSize)

	for i := 0; i < len(structs)/batchSize+1; i++ {
		low, high := i*batchSize, (i+1)*batchSize
		if high > len(structs) {
			high = len(structs)
		}
		batch := structs[low:high]
		if err := DB.InsertMulti(batch...); err != nil {
			return err
		}
	}

	return nil
}
