package reform_repository

import (
	"database/sql/driver"
	"fmt"
	"reflect"
	"sort"
	"strconv"
	"strings"
)

type NumberList []int

func NewNumberList(list []int) NumberList {
	sort.Ints(list)
	return NumberList(list)
}

func (list NumberList) Value() (driver.Value, error) {
	escapedList := []string{}
	for _, item := range list {
		escapedList = append(escapedList, fmt.Sprintf("%v", item))
	}
	sort.Strings(escapedList)

	valueStr := fmt.Sprintf("[%s]", strings.Join(escapedList, ","))
	value := []byte(valueStr)

	return value, nil
}

func (list *NumberList) Scan(value interface{}) error {
	if isNilFixed(value) {
		*list = NumberList([]int{})
		return nil
	}
	valueStr := string(value.([]uint8))
	for _, s := range []string{"[", "\"", "]", " "} {
		valueStr = strings.ReplaceAll(valueStr, s, "")
	}
	valueStrings := []string{}
	if valueStr != "" {
		valueStrings = strings.Split(valueStr, ",")
	}
	valueNumbers := []int{}
	for _, valueString := range valueStrings {
		valueInt64, err := strconv.ParseInt(valueString, 10, 64)
		if err != nil {
			return err
		}
		valueNumbers = append(valueNumbers, int(valueInt64))
	}
	*list = NumberList(valueNumbers)
	return nil
}

// O(log N). List should be sorted ascendentally
func (list NumberList) Includes(x int) bool {
	n := len(list)
	i := sort.Search(n, func(i int) bool {
		return (list)[i] >= x
	})
	if i < n && (list)[i] == x {
		return true
	}
	return false
}

func (list NumberList) Intersection(another NumberList) NumberList {
	intersected := []int{}
	for _, candidate := range list {
		if another.Includes(candidate) {
			intersected = append(intersected, candidate)
		}
	}
	return NewNumberList(intersected)
}

func (list NumberList) Subtract(another NumberList) NumberList {
	result := []int{}
	for _, candidate := range list {
		if another.Includes(candidate) {
			continue
		}
		result = append(result, candidate)
	}
	return NewNumberList(result)
}

func (list NumberList) Equal(another NumberList) bool {
	return reflect.DeepEqual(list, another)
}
