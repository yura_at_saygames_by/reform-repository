package clickhouse_repository

import (
	"time"
)

func (repo *Repository) UpdateDatePartitionGeneric(date time.Time, items [][]interface{}) error {
	dateKey := date.Format("2006-01-02")
	return repo.UpdatePartitionGeneric(dateKey, items)
}
