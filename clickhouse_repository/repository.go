package clickhouse_repository

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
)

type Repository struct {
	SQLX             *sqlx.DB
	DatabaseName     string
	TableName        string
	CreateTableDDL   string
	Columns          []string
	TestMocks        *[]interface{}
	EnableLog        bool
	UseFinalModifier bool // When FINAL is specified, ClickHouse fully merges the data before returning the result and thus performs all data transformations that happen during merges for the given table engine.
}

func (repo *Repository) IsTestModeOn() bool {
	isEnv := strings.ToUpper(os.Getenv("ENV")) == "TEST" || os.Getenv("TEST") == "1"
	return isEnv // && repo.TestMocks != nil
}

func (repo *Repository) GetTestMocks() *[]interface{} {
	if repo.TestMocks != nil {
		return repo.TestMocks
	}
	return &[]interface{}{}
}

func (repo *Repository) Mock(mocks []interface{}) error {
	repo.TestMocks = &mocks
	return nil
}

func (repo Repository) ComposeSelectQuery(queryBuilder QueryBuilder) string {
	QUERY := ""

	// SELECT
	{
		finalModifier := ""
		if repo.UseFinalModifier {
			finalModifier = " FINAL "
		}

		if selectFields := queryBuilder.Select(); selectFields != nil {
			QUERY = fmt.Sprintf("SELECT %v FROM `%v`.`%v` %v", strings.Join(*selectFields, ", "), repo.DatabaseName, repo.TableName, finalModifier)
		} else {
			QUERY = fmt.Sprintf("SELECT `%v`.* FROM `%v`.`%v` %v", repo.TableName, repo.DatabaseName, repo.TableName, finalModifier)
		}
	}

	if join := queryBuilder.Join(); join != nil {
		QUERY += " " + *join
	}

	if where := queryBuilder.Where(); len(where) > 0 {
		QUERY += " WHERE " + where
	}
	if groupBy := queryBuilder.GroupBy(); groupBy != nil && len(*groupBy) > 0 {
		QUERY += " GROUP BY " + strings.Join(*groupBy, ",")
	}
	if order := queryBuilder.OrderBy(); order != nil {
		QUERY += " ORDER BY " + *order
	}

	if pagination := queryBuilder.Pagination(); pagination != nil {
		offset := (pagination.Page - 1) * pagination.PerPage
		QUERY += fmt.Sprintf(" LIMIT %v OFFSET %v", pagination.PerPage, offset)
	}

	return QUERY
}

// rows - as [ [1, 'alice', 22], [2, 'bob', 33] ] with column order defined in repo.Columns
//
func (repo Repository) BulkInsertGeneric(rows [][]interface{}) error {
	// [TEST MOCK SYSTEM INTERCEPTION]
	if repo.IsTestModeOn() {
		return fmt.Errorf("[Repository] Attempt to bulk insert %v rows in TEST env", len(rows))
	}
	// [END OF TEST MOCK SYSTEM INTERCEPTION]

	tx, err := repo.SQLX.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback() // The rollback will be ignored if the tx has been committed later in the function

	// Preparing bulk insert sql statement
	insertSQLTemplate := ""
	{
		questions := make([]string, len(repo.Columns))
		for i := range questions {
			questions[i] = "?"
		}

		insertSQLTemplate = fmt.Sprintf(
			"INSERT INTO `%s`.`%s` (%s) VALUES (%s)",
			repo.DatabaseName,
			repo.TableName,
			strings.Join(repo.Columns, ", "),
			strings.Join(questions, ", "),
		)
	}

	stmt, err := tx.Prepare(insertSQLTemplate)
	if err != nil {
		return err
	}
	defer stmt.Close() // Prepared statements take up server resources and should be closed after use

	for _, row := range rows {
		if _, err = stmt.Exec(row...); err != nil {
			return err
		}
	}

	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

// rows - as [ [1, 'alice', 22], [2, 'bob', 33] ] with column order defined in repo.Columns
//
func (repo Repository) BulkInsertGeneric2(rows [][]interface{}) error {
	// [TEST MOCK SYSTEM INTERCEPTION]
	if repo.IsTestModeOn() {
		return fmt.Errorf("[Repository] Attempt to bulk insert %v rows in TEST env", len(rows))
	}
	// [END OF TEST MOCK SYSTEM INTERCEPTION]

	tx, err := repo.SQLX.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback() // The rollback will be ignored if the tx has been committed later in the function

	// Preparing bulk insert sql statement
	bulkInsertSQLTemplate := ""
	{
		questions := make([]string, len(repo.Columns))
		for i := range questions {
			questions[i] = "?"
		}
		questionsString := strings.Join(questions, ", ")

		values := make([]string, len(rows))
		for i := range values {
			values[i] = fmt.Sprintf("(%v)", questionsString)
		}

		bulkInsertSQLTemplate = fmt.Sprintf(
			"INSERT INTO `%s`.`%s` (%s) VALUES %s",
			repo.DatabaseName,
			repo.TableName,
			strings.Join(repo.Columns, ", "),
			strings.Join(values, ", "),
		)
	}

	stmt, err := tx.Prepare(bulkInsertSQLTemplate)
	if err != nil {
		return err
	}
	defer stmt.Close() // Prepared statements take up server resources and should be closed after use

	args := make([]interface{}, len(repo.Columns)*len(rows))
	for i, row := range rows {
		for j, _ := range repo.Columns {
			n := i*len(repo.Columns) + j
			args[n] = row[j]
		}
	}
	if _, err = stmt.Exec(args...); err != nil {
		return err
	}

	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

func (repo Repository) DestroyWhere(qb QueryBuilder) error {
	// [TEST MOCK SYSTEM INTERCEPTION]
	if repo.IsTestModeOn() {
		return fmt.Errorf("[Repository] Attempt to destroy all with %+v in TEST env", qb)
	}
	// [END OF TEST MOCK SYSTEM INTERCEPTION]

	whereClauses := qb.Where()
	query := fmt.Sprintf("ALTER TABLE `%v`.`%v` DELETE WHERE %v", repo.DatabaseName, repo.TableName, whereClauses)

	if repo.EnableLog {
		fmt.Printf("[Clickhouse] [%v.%v] %v\n", repo.DatabaseName, repo.TableName, query)
	}
	_, err := repo.SQLX.Exec(query)
	if err != nil {
		return err
	}
	// rowsAffected, err := result.RowsAffected()

	return nil
}

func (repo *Repository) UpdateAll(columns map[string]interface{}, qb QueryBuilder) error {
	// [TEST MOCK SYSTEM INTERCEPTION]
	if repo.IsTestModeOn() {
		return fmt.Errorf("[Repository] Attempt to update %v for all with %+v in TEST env", columns, qb)
	}
	// [END OF TEST MOCK SYSTEM INTERCEPTION]

	whereClauses := qb.Where()
	updateClauses := []string{}
	for column, value := range columns {
		valueStr := ""
		switch v := value.(type) {
		case string:
			valueStr = fmt.Sprintf("'%v'", v)
		case time.Time:
			valueStr = fmt.Sprintf("'%v'", v.Format("2006-01-02"))
		default:
			valueStr = fmt.Sprintf("%v", v)
		}

		clause := fmt.Sprintf("%v = %v", column, valueStr)
		updateClauses = append(updateClauses, clause)
	}

	query := fmt.Sprintf("ALTER TABLE `%v`.`%v` UPDATE %v WHERE %v", repo.DatabaseName, repo.TableName, strings.Join(updateClauses, ", "), whereClauses)

	if repo.EnableLog {
		fmt.Printf("[Clickhouse] [%v.%v] %v\n", repo.DatabaseName, repo.TableName, query)
	}
	_, err := repo.SQLX.Exec(query)
	if err != nil {
		return err
	}

	return nil
}

func (repo Repository) DropPartition(partitionExpression string) error {
	// [TEST MOCK SYSTEM INTERCEPTION]
	if repo.IsTestModeOn() {
		return fmt.Errorf("[Repository] Attempt to drop partition %v in TEST env", partitionExpression)
	}
	// [END OF TEST MOCK SYSTEM INTERCEPTION]

	query := fmt.Sprintf("ALTER TABLE `%v`.`%v` DROP PARTITION %v", repo.DatabaseName, repo.TableName, partitionExpression)

	if repo.EnableLog {
		fmt.Printf("[Clickhouse] [%v.%v] %v\n", repo.DatabaseName, repo.TableName, query)
	}
	_, err := repo.SQLX.Exec(query)
	if err != nil {
		return err
	}
	// rowsAffected, err := result.RowsAffected()

	return nil
}

func (repo Repository) Optimize() error {
	// [TEST MOCK SYSTEM INTERCEPTION]
	if repo.IsTestModeOn() {
		return fmt.Errorf("[Repository] Attempt to run optimize in TEST env")
	}
	// [END OF TEST MOCK SYSTEM INTERCEPTION]

	query := fmt.Sprintf("OPTIMIZE TABLE `%v`.`%v` FINAL", repo.DatabaseName, repo.TableName)

	if repo.EnableLog {
		fmt.Printf("[Clickhouse] [%v.%v] %v\n", repo.DatabaseName, repo.TableName, query)
	}
	_, err := repo.SQLX.Exec(query)
	if err != nil {
		return err
	}
	// rowsAffected, err := result.RowsAffected()

	return nil
}

func (repo *Repository) Count(qb QueryBuilder) (*int, error) {
	joinSql := ""
	if qbJoin := qb.Join(); qbJoin != nil {
		joinSql = *qbJoin
	}
	query := fmt.Sprintf(
		"SELECT COUNT(`%v`.id) as count FROM %v %v WHERE %v",
		repo.TableName,
		repo.TableName,
		joinSql,
		qb.Where())

	if repo.EnableLog {
		fmt.Printf("[Clickhouse] [%v.%v] %v\n", repo.DatabaseName, repo.TableName, query)
	}

	row := repo.SQLX.QueryRow(query)
	count := 0
	switch err := row.Scan(&count); err {
	case nil:
		return &count, nil
	default:
		return nil, err
	}
}
