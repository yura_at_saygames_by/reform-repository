package clickhouse_repository

import (
	"fmt"

	"github.com/ClickHouse/clickhouse-go"
	"github.com/jmoiron/sqlx"
)

// dcs - data connection string
//
func ConnectToClickhouse(dcs string, dbName string) (*sqlx.DB, error) {
	connect, err := sqlx.Open("clickhouse", dcs)
	if err != nil {
		return nil, err
	}

	// connect.SetMaxOpenConns(3)                  // sets the maximum number of open connections to the database.
	// connect.SetMaxIdleConns(0)                  // If n <= 0, no idle connections are retained.
	// connect.SetConnMaxIdleTime(1 * time.Second) // sets the maximum amount of time a connection may be idle.
	// connect.SetConnMaxLifetime(15 * time.Minute)

	if err := connect.Ping(); err != nil {
		if exception, ok := err.(*clickhouse.Exception); ok {
			return nil, fmt.Errorf("[%d] %s \n%s\n", exception.Code, exception.Message, exception.StackTrace)
		}
		return nil, err
	}
	if _, err := connect.Exec(fmt.Sprintf("USE %s;", dbName)); err != nil {
		return nil, err
	}

	fmt.Println("[Clickhouse] Connected to %v", dcs)
	return connect, nil
}
