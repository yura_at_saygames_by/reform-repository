package clickhouse_repository

import (
	"fmt"
	"strings"
)

func (repo *Repository) UpdateSingleTableGeneric(items [][]interface{}) error {
	// Strategy B. Clickhouse merge table with data separated by date tables
	{
		sqlx := repo.SQLX
		databaseName := repo.DatabaseName
		tempTableName := fmt.Sprintf("_tmp_%v", repo.TableName)
		finalTableName := fmt.Sprintf("%v", repo.TableName)

		tempTableCreateTableDDL := repo.CreateTableDDL
		{
			for what, into := range map[string]string{
				"[TABLE_NAME]": tempTableName,
			} {
				tempTableCreateTableDDL = strings.ReplaceAll(tempTableCreateTableDDL, what, into)
			}
		}

		for _, query := range []string{
			fmt.Sprintf("DROP TABLE IF EXISTS `%v`.`%v`", databaseName, tempTableName),
			tempTableCreateTableDDL,
		} {
			fmt.Println(">>> Clickhouse: ", query)
			if _, err := sqlx.Exec(query); err != nil {
				return err
			}
		}

		tempTableRepo := repo
		tempTableRepo.TableName = tempTableName

		if err := tempTableRepo.BulkInsertGeneric(items); err != nil {
			return err
		}

		for _, query := range []string{
			fmt.Sprintf("DROP TABLE IF EXISTS `%v`.`%v`", databaseName, finalTableName),
			fmt.Sprintf("RENAME TABLE `%v`.`%v` TO `%v`.`%v`", databaseName, tempTableName, databaseName, finalTableName),
		} {
			fmt.Println(">>> Clickhouse: ", query)
			if _, err := sqlx.Exec(query); err != nil {
				return err
			}
		}
	}

	return nil
}
