package clickhouse_repository

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func RunHttpQuery(user string, password string, host string, query string) (*string, error) {
	url := fmt.Sprintf("http://%v:%v@%s:8123/", user, password, host)

	request, err := http.NewRequest("POST", url, strings.NewReader(query))
	if err != nil {
		return nil, err
	}

	client := http.Client{}

	resp, err := client.Do(request)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		data, _ := ioutil.ReadAll(resp.Body)
		return nil, fmt.Errorf(string(data))
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	bodyString := string(bodyBytes)
	return &bodyString, nil
}
