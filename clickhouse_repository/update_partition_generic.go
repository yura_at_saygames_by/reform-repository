package clickhouse_repository

import (
	"fmt"
	"strings"
)

func (repo *Repository) UpdatePartitionGeneric(key string, items [][]interface{}) error {
	// Strategy A. Simple SQL. Desroy + insert
	// if false {
	// 	// Hard sync - destroy previous
	// 	if err := Repo.DestroyAll(repo.QueryBuilder{Date: &date}); err != nil {
	// 		return err
	// 	}

	// 	// Bulk insert
	// 	if err := BulkInsert(items); err != nil {
	// 		return err
	// 	}
	// }

	// Strategy B. Clickhouse merge table with data separated by date tables
	{
		sqlx := repo.SQLX
		databaseName := repo.DatabaseName
		tempTableName := fmt.Sprintf("_tmp_%v_%v", repo.TableName, key)
		finalTableName := fmt.Sprintf("%v_%v", repo.TableName, key)

		tempTableCreateTableDDL := repo.CreateTableDDL
		{
			for what, into := range map[string]string{
				"[TABLE_NAME]": tempTableName,
			} {
				tempTableCreateTableDDL = strings.ReplaceAll(tempTableCreateTableDDL, what, into)
			}
		}

		for _, query := range []string{
			fmt.Sprintf("DROP TABLE IF EXISTS `%v`.`%v`", databaseName, tempTableName),
			tempTableCreateTableDDL,
		} {
			fmt.Println(">>> Clickhouse: ", query)
			if _, err := sqlx.Exec(query); err != nil {
				return err
			}
		}

		tempTableRepo := repo
		tempTableRepo.TableName = tempTableName

		if err := tempTableRepo.BulkInsertGeneric(items); err != nil {
			return err
		}

		for _, query := range []string{
			fmt.Sprintf("DROP TABLE IF EXISTS `%v`.`%v`", databaseName, finalTableName),
			fmt.Sprintf("RENAME TABLE `%v`.`%v` TO `%v`.`%v`", databaseName, tempTableName, databaseName, finalTableName),
		} {
			fmt.Println(">>> Clickhouse: ", query)
			if _, err := sqlx.Exec(query); err != nil {
				return err
			}
		}
	}

	return nil
}
