module bitbucket.org/yura_at_saygames_by/reform-repository

go 1.18

require (
	github.com/ClickHouse/clickhouse-go v1.5.4
	github.com/go-sql-driver/mysql v1.6.0
	gopkg.in/reform.v1 v1.5.1
)

require (
	github.com/cloudflare/golz4 v0.0.0-20150217214814-ef862a3cdc58 // indirect
	github.com/jmoiron/sqlx v1.3.4
)
