# Golang Reform Repository

### Repository

```go
GetAll(queryBuilder QueryBuilder) (*reform.Struct, error)
GetAll_2(queryBuilder QueryBuilder) (*reform.Struct, error)
GetFirst(queryBuilder QueryBuilder) (*reform.Struct, error)
Save(record reform.Record) error
BulkInsert(structs []reform.Struct, batchSize int) error
Destroy(record reform.Record) error
DestroyAll(queryBuilder QueryBuilder) error
UpdateAll(record reform.Record, columns []string, queryBuilder QueryBuilder) error
DestroyAllWithoutDeadlock(queryBuilder QueryBuilder) error
```

### Query Builder

```go
interface {
	Select() *[]string
	Where() string
	Join() *string
	OrderBy() *string
	GroupBy() *[]string
	Pagination() *struct {
		Page    int
		PerPage int
	}
}
```

### StringList

```go
type StringList []string
Value() (driver.Value, error)
Scan(value interface{}) error
Includes(x string) bool
Intersection(another StringList) StringList
Subtract(another StringList) StringList
Equal(another StringList) bool
```

### Usage

```go
type MyModelRepository struct {
	database.Repository
}

func (repo *MyModelRepository) Get(queryBuilder QueryBuilder) ([]MyModel, error) {
	records, err := repo.GetAll(queryBuilder)
	if err != nil {
		return nil, err
	}
	models, err := repo.UnmarshalRecords(records)
	if err != nil {
		return nil, err
	}
	return models, nil
}

func (repo *MyModelRepository) UnmarshalRecord(record *reform.Struct) (MyModel, error) {
	if record == nil {
		return MyModel{}, nil
	}
	model, ok := (*record).(*MyModel)
	if ok {
		return *model, nil
	} else {
		return MyModel{}, fmt.Errorf("Can not cast %+v to MyModel", record)
	}
}

func (repo *MyModelRepository) UnmarshalRecords(records *[]reform.Struct) ([]MyModel, error) {
	models := []MyModel{}
	for _, record := range *records {
		model, err := repo.UnmarshalRecord(&record)
		if err != nil {
			return []MyModel{}, err
		} else {
			models = append(models, model)
		}
	}
	return models, nil
}
var myRepo = MyModelRepository{
  database.Repository{
    DB:   SQL_DB_CLIENT,
    View: MODEL_REFORM_VIEW,
  },
}
```